import { BrowserRouter, Route, Routes } from "react-router-dom";
import Nav from './Nav';
import AttendeesList from './attendees/AttendeesList';
import LocationForm from './locations/LocationForm';
import LocationList from './locations/LocationList';
import LocationDetail from './locations/LocationDetail';
import ConferenceForm from './conferences/ConferenceForm';
import AttendConferenceForm from './attendees/AttendConferenceForm';
import PresentationForm from "./presentations/PresentationForm";
import MainPage from "./MainPage";

function App(props) {
  
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="locations">
          <Route index element={<LocationList />} />
          <Route path=":id" element={<LocationDetail />} />
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
        </Route>
        <Route path="attendees">
          <Route index element={<AttendeesList />} />
          <Route path="new" element={<AttendConferenceForm />} />
        </Route>
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
